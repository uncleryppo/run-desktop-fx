package de.ithappens.run.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Calendar;

/**
 * Copyright: 2020
 * Organization: IT-Happens.de
 *
 * @author Ryppo
 */
public class Competition extends CompetitionAsset {

    @Getter @Setter
    private String name;
    @Getter @Setter
    private Calendar startOfEvent, endOfEvent;

    private String visibleName;

    public Competition(String name, String visibleName) {
        this.name = name;
        this.visibleName = visibleName;
    }

    @Override
    public String getVisibleName() {
        return visibleName;
    }

    @Override
    public Type getAssetType() {
        return Type.COMPETITION;
    }
}
