package de.ithappens.run.model;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright: 2020
 * Organization: IT-Happens.de
 *
 * @author Ryppo
 */
public abstract class CompetitionAsset {

    public enum Type{COMPETITION, PARTICIPANT, DISCIPLINE, PARTICIPATION, LIST_OF_PARTICIPANTS, LIST_OF_DISCIPLINES, UNKNOWN}
    @Getter
    private final List<CompetitionAsset> children = new ArrayList<>();

    @Override
    public String toString() {
        return getVisibleName();
    }

    public boolean hasChildren() {
        return children.size() > 0;
    }

    public abstract String getVisibleName();

    public abstract Type getAssetType();
}
