package de.ithappens.run.model;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

/**
 * Copyright: 2020
 * Organization: IT-Happens.de
 *
 * @author Ryppo
 */
public class Participant extends CompetitionAsset {

    @Getter @Setter private String startNumber;
    @Getter @Setter private Contact person;

    public Participant(String _startNumber, Contact _person) {
        startNumber = _startNumber;
        person = _person;
    }

    @Override
    public String getVisibleName() {
        return "[" + StringUtils.defaultString(startNumber) + "] " + StringUtils.defaultString(person != null ? person.getName() : null);
    }

    @Override
    public Type getAssetType() {
        return Type.PARTICIPANT;
    }
}
