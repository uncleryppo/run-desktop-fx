package de.ithappens.run.model;

/**
 * Copyright: 2020
 * Organization: IT-Happens.de
 *
 * @author Ryppo
 */
public class Disciplines extends CompetitionAsset {

    @Override
    public String getVisibleName() {
        return "Läufe";
    }

    @Override
    public Type getAssetType() {
        return Type.LIST_OF_DISCIPLINES;
    }
}
