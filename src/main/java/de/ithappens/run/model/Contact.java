package de.ithappens.run.model;

import org.apache.commons.lang3.StringUtils;

public record Contact(String id, String firstname, String lastname) {

    public Contact(String id) {
        this(id, null, null);
    }

    public String getName() {return StringUtils.defaultString(firstname) + " " + StringUtils.defaultString(lastname);}

}
