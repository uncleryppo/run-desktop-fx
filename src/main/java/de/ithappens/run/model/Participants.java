package de.ithappens.run.model;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Copyright: 2020
 * Organization: IT-Happens.de
 *
 * @author Ryppo
 */
public class Participants extends CompetitionAsset {

    @Override
    public String getVisibleName() {
        return "Teilnehmer-Liste";
    }

    @Override
    public Type getAssetType() {
        return Type.LIST_OF_PARTICIPANTS;
    }

    public List<Participant> collectParticipants() {
        List<Participant> participants = new ArrayList<>();
        getChildren().forEach(competitionAsset -> {
            if (competitionAsset instanceof Participant) {
                participants.add((Participant) competitionAsset);
            }
        });
        return participants;
    }

    public List<Contact> collectContacts() {
        List<Contact> contacts = new ArrayList<>();
        getChildren().forEach(competitionAsset -> {
            if (competitionAsset instanceof Participant) {
                contacts.add(((Participant) competitionAsset).getPerson());
            }
        });
        return contacts.size() > 0 ? contacts : null;
    }

}
