package de.ithappens.run.desktopclient;

import de.ithappens.run.model.CompetitionAsset;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;

import java.util.List;

/**
 * Copyright: 2020
 * Organization: IT-Happens.de
 *
 * @author Ryppo
 */
public class CompetitionAssetBrowser extends AssetBrowser<CompetitionAsset> {

    @FXML private ListView<CompetitionAsset> competitionAssets_lvw;

    @FXML private Label test_lbl;

    @FXML
    protected void initialize() {

    }



    @Override
    protected void bindModels() {
        test_lbl.setText("Count: " + (models != null ? models.size() : "null"));
    }

}
