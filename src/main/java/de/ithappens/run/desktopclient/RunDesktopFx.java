package de.ithappens.run.desktopclient;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class RunDesktopFx extends Application {

    private final String resourcePath = "/de/ithappens/run/desktopclient/fx/";

    public static void main (String args[]) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        System.out.println("Start " + RunDesktopFx.class);
        Parent root = FXMLLoader.load(getClass().getResource(resourcePath + "app.fxml"));
        Scene scene = new Scene(root, 300,400);
        scene.getStylesheets().add(getClass().getResource(resourcePath + "app.css").toExternalForm());
        stage.setTitle("AppWhat");
        stage.setScene(scene);
        stage.show();
    }
}