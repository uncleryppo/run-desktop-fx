package de.ithappens.run.desktopclient;

import de.ithappens.run.model.CompetitionAsset;
import javafx.css.PseudoClass;
import javafx.scene.control.TreeCell;
import org.apache.commons.lang3.StringUtils;

/**
 * Copyright: 2020
 * Organization: IT-Happens.de
 *
 * @author Ryppo
 */
public class CompetitionAssetTreeCell extends TreeCell<CompetitionAsset> {

    @Override
    protected void updateItem(CompetitionAsset competitionAsset, boolean empty) {
        super.updateItem(competitionAsset, empty);
        if (empty) {
            setText(null);
        } else {
            setText(competitionAsset.getVisibleName());
            String style = getStyleClassForAssetType(competitionAsset.getAssetType());
            if (StringUtils.isNotEmpty(style)) {
                getStyleClass().add(style);
            }
        }
    }

    public String getStyleClassForAssetType(CompetitionAsset.Type assetType) {
        return "app-sidebar-maingroup";
//        return switch (assetType) {
//            case COMPETITION -> "app-sidebar-maingroup";
//            case LIST_OF_PARTICIPANTS, LIST_OF_DISCIPLINES -> "app-sidebar-subgroup";
//            default -> null;
//        };
    }
}
