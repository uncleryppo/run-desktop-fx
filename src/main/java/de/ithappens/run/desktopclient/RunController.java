package de.ithappens.run.desktopclient;

import de.ithappens.run.model.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.ListView;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.BorderPane;
import javafx.util.Callback;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class RunController {

    private final static String resourcePath = "/de/ithappens/run/desktopclient/fx/";
    private ToolBarController toolBarComntroller;

    @FXML
    private TreeView<CompetitionAsset> sideBar_tvw;
    private TreeItem<CompetitionAsset> competitionAssetsRoot;

    @FXML
    private BorderPane appMain_borderPane;

    @FXML
    ListView<Contact> contacts_lvw;
    private ObservableList<Contact> contacts = FXCollections.observableArrayList();

    @FXML
    public void initialize() {
        try {
            appMain_borderPane.setTop(loadToolBar());
        } catch (IOException e) {
            e.printStackTrace();
        }

        sideBar_tvw.setCellFactory(new Callback<TreeView<CompetitionAsset>, TreeCell<CompetitionAsset>>() {
            @Override
            public TreeCell<CompetitionAsset> call(TreeView<CompetitionAsset> competitionAssetTreeView) {
                return new CompetitionAssetTreeCell();
            }
        });
        competitionAssetsRoot = new TreeItem<>(new CompetitionAsset() {
            @Override
            public Type getAssetType() {
                return Type.UNKNOWN;
            }

            @Override
            public String getVisibleName() {
                return "SuperItem";
            }
        });
        sideBar_tvw.setRoot(competitionAssetsRoot);
        sideBar_tvw.getSelectionModel().selectedItemProperty().addListener((observableValue, last, now) -> {
            System.out.println("selected: " + now);
            contacts_lvw.getItems().clear();
            Node node = null;
            if (now != null) {
                node = null;
                try {
                    if (now.getValue() instanceof Competition) {
                        FXMLLoader loader = createLoader("competition-browser.fxml");
                        node = loader.load();
                        CompetitionAssetBrowser browser = loader.getController();
                        browser.setModels(now.getValue() != null ? now.getValue().getChildren() : null);
                    } else if (now.getValue() instanceof Participants) {
                        FXMLLoader loader = createLoader("participants-browser.fxml");
                        node = loader.load();
                        ParticipantsBrowser browser = loader.getController();
                        browser.setModels(((Participants) now.getValue()).collectParticipants());

                        List<Contact> contacts = ((Participants) now.getValue()).collectContacts();
                        if (contacts != null) {
                            contacts_lvw.getItems().addAll(contacts);
                            contacts_lvw.setCellFactory(contactListView -> new ContactListViewCell());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            appMain_borderPane.setCenter(node);
        });

        contacts_lvw.setItems(contacts);
        contacts_lvw.setCellFactory(contactListView -> new ContactListViewCell());

        simulateData();
    }

    public Node loadToolBar() throws IOException {
        String resourcesPath = "/de/ithappens/run/desktopclient/fx/";
        ResourceBundle resourceBundle = ResourceBundle.getBundle("de.ithappens.run.desktopclient.fx.run");
        return FXMLLoader.load(getClass().getResource(resourcesPath + "app-toolbar.fxml"), resourceBundle);
    }

    private void setCompetitionAssets(List<CompetitionAsset> allAssets) {
        competitionAssetsRoot.getChildren().clear();
        if (allAssets != null && allAssets.size() > 0) {
            competitionAssetsRoot.getChildren().addAll(createTreeItems(allAssets));
        }
    }

    public List<TreeItem<CompetitionAsset>> createTreeItems(List<CompetitionAsset> assets) {
        List<TreeItem<CompetitionAsset>> treeItems = null;
        if (assets != null && assets.size() > 0) {
            treeItems = new ArrayList<>();
            for (CompetitionAsset asset : assets) {
                TreeItem<CompetitionAsset> treeItem = new TreeItem<>(asset);
                if (asset.hasChildren()) {
                    treeItem.getChildren().addAll(createTreeItems(asset.getChildren()));
                }
                treeItems.add(treeItem);
            }
        }
        return treeItems;
    }

    private void simulateData() {
        Contact c1 = new Contact("ID-2901","C", "R");
        contacts.add(c1);
        contacts.add(new Contact("ID-1511","F", "R"));
        contacts.add(new Contact("ID-0105"));

        List<CompetitionAsset> competitionAssets = new ArrayList<>();
        Competition lauf2021 = new Competition("15. Liebenburger Lauf 2021","15. Lauf 2021");
        Participants participants2021 = new Participants();
        participants2021.getChildren().add(new Participant("1",c1));
        lauf2021.getChildren().add(participants2021);
        lauf2021.getChildren().add(new Disciplines());
        competitionAssets.add(lauf2021);
        Competition lauf2019 = new Competition("14. Liebenburger Lauf 2019","14. Lauf 2019");
        lauf2019.getChildren().add(new Disciplines());
        competitionAssets.add(lauf2019);
        setCompetitionAssets(competitionAssets);
    }

    public static FXMLLoader createLoader(String fxmlFilename) {
        return new FXMLLoader(AssetBrowser.class.getResource(resourcePath + fxmlFilename));
    }
}
