package de.ithappens.run.desktopclient;

import de.ithappens.run.model.Contact;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.TitledPane;

public class ContactListViewCell extends ListCell<Contact> {

    private FXMLLoader loader;

    @FXML
    TitledPane title_tpn;
    @FXML
    Label id_lbl;

    @Override
    protected void updateItem(Contact contact, boolean empty) {
        super.updateItem(contact, empty);

        if (empty || contact == null) {
            setText(null);
            setGraphic(null);
        } else {
            if (loader == null) {
                loader = new FXMLLoader(getClass().getResource("/de/ithappens/run/desktopclient/fx/ContactCell.fxml"));
                loader.setController(this);
                try {
                    loader.load();
                    setGraphic(title_tpn);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            title_tpn.setExpanded(false);
            title_tpn.setText(contact.getName());
            id_lbl.setText(contact.id());
        }
    }
}
