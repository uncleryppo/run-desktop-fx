package de.ithappens.run.desktopclient;

import de.ithappens.run.model.CompetitionAsset;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;

import java.util.List;

/**
 * Copyright: 2020
 * Organization: IT-Happens.de
 *
 * @author Ryppo
 */
public abstract class AssetBrowser<T extends CompetitionAsset> {

    List<T> models = null;

    public void setModels(List<T> _models) {
        models = _models;
        bindModels();
    }

    protected abstract void bindModels();

}
