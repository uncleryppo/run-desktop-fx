package de.ithappens.run.desktopclient;

import de.ithappens.run.model.CompetitionAsset;
import de.ithappens.run.model.Participant;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.util.List;

/**
 * Copyright: 2020
 * Organization: IT-Happens.de
 *
 * @author Ryppo
 */
public class ParticipantsBrowser extends AssetBrowser<Participant> {

    private ObservableList<Participant> participants =
            FXCollections.observableArrayList();

    @FXML private TableView<Participant> participants_lvw;
    @FXML private TableColumn<Participant, String> participant_tbc;

    @FXML
    protected void initialize() {
        participants_lvw.setItems(participants);
        participant_tbc.setCellValueFactory(participant -> {
            return new SimpleStringProperty( participant.getValue().getPerson().getName());
        });
        System.out.println("Was soll das");
    }

    @Override
    protected void bindModels() {
        participants.clear();
        participants.addAll(models);
        System.out.println("Count of models: " + models.size());
    }
}
